/* Constant Density field to pass to the RT code
 *
 * AUTHOR: Eric Tittley
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <sys/types.h>
#include <dirent.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "config.h"

extern "C" {
#include "RT_Precision.h"
#include "CDRT.h"
#include "PMRT.h"
#include "RT.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "RT_ConstantsT.h"
#include "RT_SourceT.h"
#include "RT_Luminosity_Types.h"
#include "Support.h"
}

#ifdef CUDA
# include "RT_CUDA/RT_CUDA_Binding.h"
# include "RT_CUDA/RT_CUDA_Util.h"
# include "RT_CUDA/RT_CUDA.h"
#endif

/* Macro for freeing memory.
 * if a pointer is NULL, free does nothing. */
#define FREE_MEMORY_BASE \
  free(density);     \
  free(velocity_z);    \
  free(velocity_x);    \
  RT_Data_Free(Data[0]); \
  free(Data);        \
  free(np_list);     \
  free(pmf);         \
  free(v);           \
  free(r);           \
  free(rnge);        \

#ifdef SOURCE_TABLE
 #define FREE_MEMORY_SOURCE_TABLE \
  free(source_nu);  \
  free(source_f);
#else
 #define FREE_MEMORY_SOURCE_TABLE
#endif

#define FREE_MEMORY FREE_MEMORY_BASE \
                    FREE_MEMORY_SOURCE_TABLE


/* ----------------- The main program. ------------------------- */
int main(int argc, char **argv)
{
 int i;
 int * rnge=NULL;
 int * np_list=NULL;
 rt_float * r=NULL;
 rt_float * v=NULL;
 rt_float * pmf=NULL;
 double wtime[2];

 /* Ignored if CONSTANT_DENSITY_COMOVING not defined */
 rt_float aa=1., z=0.0;
 rt_float const a_f=1.0;

 /* For Radiative Transfer */
 int ierr;
 rt_float t, dt;

 /* Gas density field */
 rt_float *density = NULL;
 rt_float *velocity_z = NULL;
 rt_float *velocity_x = NULL;
 /* Data structure for the ionisation state for each Line of Sight */
 RT_Data ** Data = NULL;          /* All the data. */

 char hostname[BUFSIZ];
 const size_t LABEL_STR_SIZE=32;
 char label_string[LABEL_STR_SIZE];

#ifdef SOURCE_NORMALISE_SPECTRUM
 rt_float sum=0.0,nu,dnu,L_0_new;
#endif

 /* The run-time parameters */
 ParametersT Parameters;

 /* Check the arguments */
 if (argc < 2) {
  for(i=0;i<argc;i++) {
   printf("%s ",argv[i]);
  }
  printf("\n");
  printf("Usage: CDRT start\n");
  printf("   or: CDRT backup\n");
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",__LINE__);
 }

#ifdef CUDA
// ierr=RT_CUDA_InitialiseGPU(argc, argv);
 ierr=RT_CUDA_InitialiseDevices();
 PMRT_ErrorCheck(ierr,"Unable to initialise GPU",__LINE__);
 /* Over-rule what the function found */
 max_cuda_devs = 1;
 cuda_device_ids[0]=0;
#ifdef __cplusplus
 cudaDeviceProp deviceProp;
#else
 struct cudaDeviceProp deviceProp;
#endif
 cudaGetDeviceProperties(&deviceProp, cuda_device_ids[0]);
 printf("OVERRULE. Use device %i\n",cuda_device_ids[0]);
 RT_CUDA_Util_printDeviceProp(deviceProp);
#endif

 /****** RUN STATUS INFO ******/ 

 /* Print the host name of this process. */
 memset(hostname, 0, BUFSIZ);
 ierr=gethostname(hostname, BUFSIZ);
 PMRT_ErrorCheck(ierr,"gethostname failed",__LINE__);
 printf("%s (PID=%i)\n", hostname, getpid());
 ierr=fflush(stdout);

 /* Repeat the command line */
 printf("%s %s\n",argv[0],argv[1]);
#ifdef _OPENMP
#pragma omp parallel
 { printf(" OMP_NUM_THREADS = %i\n", omp_get_num_threads()); }
#endif
 printf("\n");
 /* Display the configuration at compile time */
 ierr = ShowConfig();
 PMRT_ErrorCheck(ierr,"Check the compile-time configuration",__LINE__);

 /****** END OF RUN STATUS INFO ******/ 

 /****** Read in the Parameters ******/
 printf("Reading Parameter File\n");
 ierr=CDRT_ReadParameterFile("CDRT.param", &Parameters);
 PMRT_ErrorCheck(ierr,"ReadParameterFile failed",__LINE__);

 /************* Set Constants structure *****************/
 RT_ConstantsT Constants;
 ierr = RT_ReadConstantsFile("Constants.params",&Constants);
 /* Set Constants.Alpha */
 RT_InitializeCrossSectionArray(&Constants);

/************** THE FOLLOWING SHOULD BE IN THE PARAMETER FILE ***************/
 /* Set up Source */
 RT_SourceT Source;
 Source.z_on  = 1e30;
 Source.z_off = -0.1;
 Source.AttenuationGeometry = SphericalWave;
 /*  100,000 K Black Body */
 Source.SourceType = SourceBlackBody;
 /* We need 5e48 ionizing photons per second */
 /* m^2 (surface area of source) */
 //Source.SurfaceArea = 3.29e18; /* According to Sam */
 Source.SurfaceArea = 4.677811703730327e+18; /* According to Mathematica */
 Source.T_BB  = 1e5; /* K */

 RT_SetSourceFunction(&Source); /* Set the pointer to the LuminosityFunction */
 /***************************************************************************/

 wtime[0] = Wtime();

 /* Radiative Transfer: Allocate memory for large arrays */
 Data = RT_Data_AllocateGridMemory(Parameters.NLOS, Parameters.NFLUX);
 if (Data==NULL) {
  PMRT_ErrorCheck(EXIT_FAILURE,"Unable to allocate memory for RT data",__LINE__);
 }

 /* *** Initialise the data *** */
 /* In the following, aa & z are only used if CONSTANT_DENSITY_COMOVING is
  * defined. CONSTANT_DENSITY_COMOVING is currently disabled.
  */
 /* Distance vector [m] */
 ierr = CDRT_SetR(aa, Parameters, Constants, Data);
 /* Density [kg/m^3] */
 ierr = CDRT_SetRho(z, z, Parameters, Constants, Data);
 /* Fractions */
 ierr = CDRT_SetFractions(Parameters, Data);
 /* Temperature [K] (need to set fractions, first) */
 ierr = CDRT_SetT(Parameters, Constants, Data);

 /* Initialise the source spectrum */
 if( RT_InitialiseSpectrum(0, Constants, &Source) == EXIT_FAILURE ) {
  PMRT_ErrorCheck(EXIT_FAILURE,"Problem with the source spectrum",__LINE__);
 }

 /* Sources are timed by redshift, but constant-density runs are timed from
  * t=0 (z=inf).
  * So we need to calculate an offset in time to z_on. 
  * In the RT_CUDA_* code, this is t_on */
  rt_float timeOffsetTo_z_on = cosmological_time( 1.0 / (1.0 + Source.z_on),
                                                 Constants );

 /* *********************************************************************
  * ****************** The Radiative Transfer code **********************
  * *********************************************************************
  *
  * t		Time at this point in the simulation
  * dt  	Timestep
  * Data	Structure containing the status of the gas.
  */
#if VERBOSE >= 1
 printf("%s: Beginning of RT section.\n",__FILE__); ierr=fflush(stdout);
#endif
 dt = Parameters.TimeDumpInterval*Parameters.TimeUnit; /* s */
 for(t=0.0;
     t<Parameters.TimeTotal*Parameters.TimeUnit*Constants.OneMinusEps;
     t+=dt) {
  printf("t=%8.6e; dt=%6.4e;\n", t, dt);
  ierr=fflush(stdout);
 #ifdef DEBUG
  if(dt < 0.) {
   printf("ERROR: %s: %i: dt < 0: dt=%f; t=%e\n",__FILE__,__LINE__,dt,t);
   PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",__LINE__);
  }
 #endif

 #ifdef CONSTANT_DENSITY_COMOVING
  /* CURRENTLY DISABLED */
  /* R and density will need updating because of expansion */
  ierr = CDRT_SetR(1, a_f, Data, Parameters);
  ierr = CDRT_SetRho(1, z, z_f, Data);
 #endif

  /* Do the radiative transfer.
   * RT expects the expansion factor at the *end* of the iteration, since it
   * uses it for calculations at the end of the dt increment. */
 #if VERBOSE >= 1
  printf("Calling RT...\n"); ierr=fflush(stdout);
 #endif
 #ifdef CUDA
  ierr = RT_CUDA_DistributeLOSs(Parameters.NLOS,
                                t + timeOffsetTo_z_on,
                                dt,
                                a_f,
                                Source,
                                Constants,
                                Data);
 #else
  ierr = RT_DistributeLOSs(Parameters.NLOS,
                           t + timeOffsetTo_z_on,
                           dt,
                           a_f,
                           Source,
                           Constants,
                           Data);
 #endif
  PMRT_ErrorCheck(ierr,"RT failed",__LINE__);

  /* We are at a time for which a dump was requested, so dump */
  const char *Dir = "save";
 #ifdef LONG_FILE_LABEL
  ierr=snprintf(label_string, LABEL_STR_SIZE-1, "%s/Data_t=%09.6f", Dir,
        	(t+dt)/Parameters.TimeUnit);
 #else
  ierr=snprintf(label_string, LABEL_STR_SIZE-1, "%s/Data_t=%06.2f", Dir,
        	(t+dt)/Parameters.TimeUnit);
 #endif
  if(ierr>(int)LABEL_STR_SIZE) {
   PMRT_ErrorCheck_File(EXIT_FAILURE,"Path too long",Dir,__LINE__);
  }
  printf("CDRT: dumping RT to file %s\n",label_string);
  RT_FileHeaderT FileHeader;
  FileHeader.ExpansionFactor = 1.0;
  FileHeader.Redshift        = 0.0;
  FileHeader.Time            = t+dt;
  ierr = RT_SaveData(label_string, Data, 1, FileHeader);
  PMRT_ErrorCheck_File(ierr,"Unable to write dump file",
        	       label_string,__LINE__);

  /* Output diagnostics */
  wtime[1] = Wtime();
  printf("CDRT: End of iter: t=%8.6e dt=%6.4e elapsed=%8.1f\n",
          t, dt, wtime[1]-wtime[0]);
  ierr=fflush(stdout);
 } /* end of t = 0 to TimeTotal */
 /* *************************************************************************
  * *** End of Radiative Transfer section ***********************************
  * *************************************************************************/

 wtime[1] = Wtime();
 printf("Elapsed wall time: %.1f sec.\n\n", wtime[1] - wtime[0]);
 ierr=fflush(stdout);

 /* Free allocated memory */
 FREE_MEMORY;

 printf("Done!\n");
 ierr=fflush(stdout);

 return EXIT_SUCCESS; /* YAY! */
}
