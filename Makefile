ROOT_DIR := $(CURDIR)

include make.config

LOCAL_LIBS = libCDRT/libCDRT.a libPMRT/libPMRT.a RT/libRT.a \
             Support/libSupport.a libPF/libPF.a

LIBS = $(LOCAL_LIBS) $(LIB_ARCH) -lm

ifeq ($(CUDA),1)
 LOCAL_LIBS += RT_CUDA/libRT_CUDA.a
endif

SUBDIRS = libCDRT libPMRT RT Support libPF
ifeq ($(CUDA),1)
 SUBDIRS += RT_CUDA
endif

CDRT_SRC = libCDRT/CDRT_SetR.c \
           libCDRT/CDRT_SetRho.c \
           libCDRT/CDRT_SetT.c \
           libCDRT/CDRT_SetFractions.c 

PMRT_SRC = libPMRT/PMRT_ErrorCheck.c \
           libPMRT/PMRT_ErrorCheck_File.c

SUPPORT_SRC = Support/ReportFileError.c Support/mymalloc.c Support/ShowConfig.c \
              Support/Create_PM_Communicator.c

#DEPS = dep.mak RT/dep.mak Support/dep.mak

INCS = $(INCS_BASE) $(INC_CDRT) $(INC_PMRT) $(INC_RT) $(INC_SUPPORT)

ifeq ($(CUDA),1)
 INCS += $(INC_RT_CUDA) $(NVCC_INC)
endif

MAKE_CONFIG = $(ROOT_DIR)/make.config

all: $(EXEC)

$(EXEC): $(LOCAL_LIBS) CDRT.o
	$(LD) -o $(EXEC) $(LD_FLAGS) CDRT.o $(LIBS)

install:
	mv $(EXEC) ../

libCDRT/libCDRT.a: $(SUBDIRS) $(CDRT_SRC) $(DEPS) RT/RT.h
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C libCDRT

libPMRT/libPMRT.a: $(PMRT_SRC) $(DEPS) RT/RT.h
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C libPMRT

RT/libRT.a: $(RT_SRC) $(DEPS) RT/RT.h
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C RT

RT_CUDA/libRT_CUDA.a:
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C RT_CUDA

libPF/libPF.a: libPF
	$(MAKE) CC=$(CC) CFLAGS="$(CFLAGS)" -C libPF

Support/libSupport.a: $(SUPPORT_SRC) $(DEPS) RT/libRT.a
	$(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C Support

clean:
	-rm *.o
	-rm *~
	-rm Make.log
	-rm CDRT_SVN_REVISION.h
	-rm -f splint.log
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C $$dir clean ) \
	done

distclean: clean
	-rm dep.mak
	-rm $(EXEC)
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) ROOT_DIR=${ROOT_DIR} MAKE_CONFIG=$(MAKE_CONFIG) -C $$dir distclean ) \
	done

#$(DEPS): RT libPF CDRT_SVN_REVISION.h RT/RT_SVN_REVISION.h
#	for dir in $(SUBDIRS); do \
#	 ( $(MAKE) -C $$dir dep ) \
#	done
#	$(DEPEND) $(DEPEND_FLAGS) $(INCS) $(DEFS) CDRT.cpp > dep.mak

check:
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir check ) \
	done
	splint +posixlib -nullderef -nullpass $(INCS) $(DEFS) CDRT.cpp >> splint.log

RT:
	git clone git@bitbucket.org:eric_tittley/rt.git
	mv rt RT

RT_CUDA:
	git clone git@bitbucket.org:eric_tittley/rt_cuda.git
	mv rt_cuda RT_CUDA

libPF:
	git clone git@bitbucket.org:eric_tittley/libpf.git
	mv libpf libPF

update:
	cd RT; git pull
	cd RT_CUDA; git pull
	cd libPF; git pull
	git pull

status:
	cd RT; git status
	cd RT_CUDA; git status
	cd libPF; git status
	git status

push:
	cd RT; git push
	cd RT_CUDA; git push
	cd libPF; git push
	git push

#include dep.mak
