/* Create the MPI communicator for all processes that deal with the particle (PM)
 * data.  Limited the communicator to the first 2^n processes, including the
 * MASTER.
 *
 * int Create_PM_Communicator( MPI_Comm *PM_Comm, int *num_PM_procs );
 *
 * ARGUMENTS
 *  Input and initialised
 *   PM_Comm	Pointer to a communicator already declared (memory allocated).
 *   num_PM_procs	Set to the number of processes in the PM_Comm.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE  Unable to allocate memory.
 *
 * NOTES
 * -Returns MPI_COMM_WORLD if the total number of processes is a power of 2.
 *  MPI_COMM_WORLD cannot be free'd with MPI_Comm_free(), so only free PM_Comm
 *  if num_PM_procs != nproc.
 * -num_PM_procs can be retrieved independently via:
 *   MPI_Comm_size(PM_Comm,&num_PM_procs);
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  06 04 17 First version.
 *  08 08 04 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 */
 
#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifndef M_LN2
# define M_LN2		0.69314718055994530942	/* log_e 2 */
#endif
#include <mpi.h>

#include "Support.h"
#include "RT_constants_configuration.h"

int Create_PM_Communicator( MPI_Comm *PM_Comm, int *num_PM_procs ) {

 int numprocs=0, me=-1, i;
 int *ranks=NULL;
 MPI_Group world_group, PM_group;

 /* Get the total number of processors */
 (void)MPI_Comm_size( MPI_COMM_WORLD, &numprocs);
 (void)MPI_Comm_rank( MPI_COMM_WORLD, &me);

 /* Get the largest power of 2 that is not larger than numprocs.
  * Add a small number before floor'ing. */
 *num_PM_procs = (int)pow(2.,floor(log((double)numprocs)/M_LN2+1e-3));

 /* Print a warning if num_PM_procs != numprocs and then create a communicator
  * for the PM code which has only 2^n processes */
 if(*num_PM_procs == numprocs ) {
  *PM_Comm = MPI_COMM_WORLD;
 } else {

  if(me==MASTER) {
   printf("WARNING: Create_PM_Communicator: The number of processes is not a power of 2.\n");
   printf("         This is not fatal, but the PM component will only use %i of %i\n",
          *num_PM_procs, numprocs);
   printf("         processes.  The RT component will use all the processes.\n");
   (void)fflush(stdout);
  }

  /* Set the processes that will be in PM_Comm. */
  ranks = (int *)malloc(*num_PM_procs*sizeof(int));
  if(ranks==NULL) {
   printf("ERROR: Create_PM_Communicator: Failed to allocate memory for ranks.\n");
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
  for(i=0;i<*num_PM_procs;i++) {
   ranks[i] = i;
  }

  /* Create a group for all the processes */
  (void)MPI_Comm_group( MPI_COMM_WORLD, /*@out@*/&world_group );

  /* Create the PM group */
  (void)MPI_Group_incl( world_group, *num_PM_procs, ranks, /*@out@*/&PM_group );

  /* Create the Communicator for the PM_group */
  (void)MPI_Comm_create( MPI_COMM_WORLD, PM_group, PM_Comm );

  /* Free memory */
  (void)free(ranks);
  (void)MPI_Group_free(&world_group);
  (void)MPI_Group_free(&PM_group);
 }

 return EXIT_SUCCESS;
}
