/* Show the configurations with which CDRT was compiled.
 *
 * AUTHOR: Eric Tittley
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>

#include "Support.h"

#include "RT_Data.h"

#include "CFLAGS.h"

int ShowConfig(void) {

 printf("**** Compile-time options ****\n");

 /* The optimization flags with which the code was compiled */
 printf("CC = %s\n",CC);

 /* The optimization flags with which the code was compiled */
 printf("CFLAGS = %s\n",CFLAGS);

#ifdef DEBUG
 printf("DEBUG\n");
#endif

 printf("VERBOSE = %i\n", VERBOSE);

#ifdef TIME_CODE_SEGMENTS
 printf("TIME_CODE_SEGMENTS\n");
#endif

/* The parallelisation method, if any */
#ifdef RT_SERIAL
 printf("RT_SERIAL: Code compiled for serial execution.\n");
#endif
#ifdef RT_OPENMP
 printf("RT_OPENMP: Code compiled for parallel execution using shared memory.\n");
#endif
#if   (defined RT_SERIAL) && (defined RT_OPENMP)
 printf("Can enable only one of RT_SERIAL & RT_OPENMP\n");
 return EXIT_FAILURE; 
#endif
#if   !(defined RT_SERIAL) && !(defined RT_OPENMP)
 printf("One of RT_SERIAL, RT_MPI or RT_OPENMP must be set.\n");
 return EXIT_FAILURE; 
#endif

#ifdef FORCE_REDSHIFT_STOP
 printf("WARNING: Forcibly halting at a redshift of %f\n",FORCE_REDSHIFT_STOP);
#endif

#ifdef CONSTANT_DENSITY_COMOVING
#ifndef CONSTANT_DENSITY
 printf("ERROR: Defined CONSTANT_DENSITY_COMOVING but not CONSTANT_DENSITY");
 return EXIT_FAILURE;
#endif
 printf("CONSTANT_DENSITY_COMOVING. The density will be scaled by a^-3.\n");
#endif

#ifdef REFINEMENTS
 printf("REFINEMENTS\n");
#ifdef USE_MCTfR
 printf(" USE_MCTfR is set.\n");
#endif
#endif

#ifdef THIN_APPROX
 printf("THIN_APPROX: Thin approximation. Spectrum is not modified by absorption in the column.\n");
#endif

#if !( (defined INTEGRATE_ODE_EULER) || (defined INTEGRATE_ODE_RUNGE_KUTTA) )
 printf("ERROR: Either INTEGRATE_ODE_EULER or INTEGRATE_ODE_RUNGE_KUTTA must be defined.\n");
 return EXIT_FAILURE;
#endif
#if (defined INTEGRATE_ODE_EULER) && (defined INTEGRATE_ODE_RUNGE_KUTTA)
 printf("ERROR: Only one of INTEGRATE_ODE_EULER or INTEGRATE_ODE_RUNGE_KUTTA can be defined.\n");
 return EXIT_FAILURE;
#endif
#ifdef INTEGRATE_ODE_EULER
 printf("INTEGRATE_ODE_EULER: using Euler's method for the ODE integration\n");
#endif
#ifdef INTEGRATE_ODE_RUNGE_KUTTA
 printf("INTEGRATE_ODE_RUNGE_KUTTA: using a 4th-order Runge-Kutta method for the ODE integration\n");
#endif

#ifdef ADAPTIVE_TIMESCALE
 printf("ADAPTIVE_TIMESCALE\n");
#endif

#ifdef USE_B_RECOMBINATION_CASE
 printf("USE_B_RECOMBINATION_CASE\n");
#endif

#ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
 printf("ENFORCE_MINIMUM_CMB_TEMPERATURE\n");
#endif

#ifdef SOURCE_NORMALISE_SPECTRUM
 printf("SOURCE_NORMALISE_SPECTRUM: Ignore L_0 above.\n");
#endif

#ifdef PLANE_WAVE
 printf("PLANE_WAVE: The source is not attenuated by r^-2.\n");
#else
 printf("The source is attenuated by r^-2.\n");
#endif

 printf("RT_Data is version %i.%i\n",VERSION_MAJOR,VERSION_MINOR);
#ifdef RT_DATA_SHORT
 printf(" RT_DATA_SHORT: Will not store the rates for heating, cooling, ionisation, and energy input.\n");
#endif

 printf("\n");

 (void)fflush(stdout);

 return EXIT_SUCCESS;
}
