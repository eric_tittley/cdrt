#ifndef _SUPPORT_H_
#define _SUPPORT_H_

#ifdef MPI
# include <mpi.h>
#endif
#include <stdlib.h>

void ReportFileError(const char *filename);
int ShowConfig(void);
void ShowCosmology(void);
#ifdef MPI
int Create_PM_Communicator( MPI_Comm *PM_Comm, int *num_PM_procs);
/*@null@*//*@out@*/ void *mymalloc(const size_t size);
#endif

/* Read the gas density from a file */
int read_density_file(const int me, const char *filename,  const float aa,
                      const int Ncells,
                      float *density, float *velocity_z, float *velocity_x);

double Wtime(void);

double Vunit(const double a,
             const double boxsize,
             const double Omega_o, 
             const double Omega_v,
             const double Omega_k);
#endif
