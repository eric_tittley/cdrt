#ifdef HAVE_CONFIG
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#ifdef MPI
# include <mpi.h>
#endif

#include "Support.h"

/*@null@*//*@out@*/ void *mymalloc(const size_t size)
/* Allocate memory and check if it worked. */
{
 void *tmp=NULL;
 if (size > 0) {
  tmp = malloc(size);
 #ifdef DEBUG
  if (tmp == NULL) {
   printf("ERROR: mymalloc: Unable to allocate %li bytes.\n", (long)size);
   fflush(stdout);
  #ifdef MPI
   MPI_Abort(MPI_COMM_WORLD, 1);
  #endif
   exit(1);
  }
 #endif
 }
 return (tmp);
}
