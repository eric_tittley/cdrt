/* Wtime: Time in seconds from an arbitrary point in the past
 *
 * double Wtime()
 *
 * ARGUMENTS
 *  None
 *
 * RETURNS
 *  Time in seconds since an arbitrary time in the past.
 *
 * USAGE
 *  double t0 = Wtime();
 *  MyFunction();
 *  double t_elapsed = Wtime() - t0;
 *
 * AUTHOR: Eric Tittley
 */

#include <stdlib.h>
#include <sys/time.h>
double Wtime(void)
{
 double wtime;
 struct timeval tv;
 gettimeofday(&tv, NULL);
 wtime = tv.tv_sec;
 wtime += (double)tv.tv_usec / 1000000.0;
 return wtime;
}
