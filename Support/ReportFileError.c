/*
 If a file operation produces an error, report the error.
 
 AUTHOR: Eric Tittley
 
 HISTORY
  03 09 05 First version.
  03 09 12 Added #include <stdio.h>
  04 05 24 Flush stdout.  filename is const.
  06 09 14 Declare in Support.h and add config.h
*/

#include "config.h"

#include <errno.h>
#include <stdio.h>

#include "Support.h"

void ReportFileError(const char *filename)
{
 switch (errno) {
 #ifdef EACCES
  case EACCES:
   printf("Invalid permissions to open %s.\n", filename);
   break;
 #endif
 #ifdef EINVAL
  case EINVAL:
   printf("Invalid mode in openning %s.\n", filename);
   break;
 #endif
 #ifdef EEXIST
  case EEXIST:
   printf("File %s already exists.\n", filename);
   break;
 #endif
 #ifdef EFAULT
  case EFAULT:
   printf("File %s points outside your accessible address space.\n", filename);
   break;
 #endif
 #ifdef EISDIR
  case EISDIR:
   printf("File %s is a directory.\n", filename);
   break;
 #endif
 #ifdef ELOOP
  case ELOOP:
   printf
    ("Too many symbolic links were encountered in resolving %s, or O_NOFOLLOW was specified but pathname was a symbolic link.\n",
     filename);
   break;
 #endif
 #ifdef EMFILE
  case EMFILE:
   printf("The process already has the maximum number of files open.\n");
   break;
 #endif
 #ifdef ENAMETOOLONG
  case ENAMETOOLONG:
   printf("Pathname %s is too long.\n", filename);
   break;
 #endif
 #ifdef ENFILE
  case ENFILE:
   printf
    ("The limit on the total number of files open on the system has been reached.\n");
   break;
 #endif
 #ifdef ENOENT
  case ENOENT:
   printf("%s does not exist.\n", filename);
   break;
 #endif
 #ifdef ENOMEM
  case ENOMEM:
   printf("Insufficient kernel memory was available to open %s.\n", filename);
   break;
 #endif
 #ifdef ENOSPC
  case ENOSPC:
   printf("The device containing %s has no room for the new file.\n", filename);
   break;
 #endif
 #ifdef ENOTDIR
  case ENOTDIR:
   printf("Directory component in pathname %s is not a directory.\n", filename);
   break;
 #endif
 #ifdef ENXIO
  case ENXIO:
   printf
    ("The file %s is a FIFO and no process has the file open for reading. Or, the file is a device special file and no corresponding device exists.\n",
     filename);
   break;
 #endif
 #ifdef EROFS
  case EROFS:
   printf
    ("The file %s is on a read-only filesystem and write access was requested.\n",
     filename);
   break;
 #endif
 #ifdef ETXTBSY
  case ETXTBSY:
   printf
    ("The file %s refers to an executable image which is currently being executed and write access was requested.\n",
     filename);
   break;
 #endif
  default:
   printf("Error on opening file %s.\n", filename);
 }
 (void)fflush(stdout);
}
