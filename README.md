# Constant Density Radiative Transfer: CDRT #

Perform ionization through a constant density using the RT library.

## COntributors ##
Eric Tittley

## Project Page ##
https://bitbucket.org/eric_tittley/cdrt

## Download ##
git clone git@bitbucket.org:eric_tittley/cdrt.git