#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "PMRT.h"

void PMRT_ErrorCheck( const int ierr,
                      const char *Msg,
		      const int LINE )
{
 int ierr_dum;
 if(ierr != EXIT_SUCCESS) {
  printf("ERROR: CDRT: %i: %s\n",LINE,Msg);
  ierr_dum=fflush(stdout);
  exit(EXIT_FAILURE);
 }
}

