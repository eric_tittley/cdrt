#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "PMRT.h"

void PMRT_ErrorCheck_File( const int ierr,
                           const char *Msg,
			   const char *File,
		           const int LINE )
{
 if(ierr != EXIT_SUCCESS) {
  printf("ERROR: PMRT: %i: %s: File: %s\n",LINE,Msg,File);
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",LINE);
 }
}
