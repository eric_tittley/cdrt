#ifndef _PMRT_H_
#define _PMRT_H_

void PMRT_ErrorCheck(int  const  ierr,
                     char const *Msg,
		     int  const  LINE );

void PMRT_ErrorCheck_File(int  const  ierr,
                          char const *Msg,
			  char const *File,
		          int  const  LINE );

double PMRT_GetNewTimeStep(double  const  aa,
                           double  const  DTol,
                           double  const  af,
                           int     const  iout,
                           double  const *aout );

#endif
