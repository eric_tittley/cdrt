#include <stdlib.h>
#include <math.h>

#include "CDRT.h"
#include "RT_Data.h"
#include "RT_ConstantsT.h"

int CDRT_SetT(ParametersT const Parameters,
              RT_ConstantsT const  Constants,
              RT_Data ** const Data) {

 size_t i,j;
 for (j=0;j<Parameters.NLOS;j++) {
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->T[i]    = Parameters.To;
   /* Set the Entropy from the fixed temperature */
   const rt_float n_H2 =Data[j]->n_H[i] *Data[j]->f_H2[i];
   const rt_float n_He2=Data[j]->n_He[i]*Data[j]->f_He2[i];
   const rt_float n_He3=Data[j]->n_He[i]*Data[j]->f_He3[i];
   /* Electron density */
   const rt_float n_e = n_H2 + n_He2 + (2.0 * n_He3);
   /* Total number density */
   const rt_float n = Data[j]->n_H[i] + Data[j]->n_He[i] + n_e;
   /* Assumes m_He = 4 m_H and mass_e = 0 */
   const rt_float mu = (Data[j]->n_H[i] + 4.*Data[j]->n_He[i])/n;
   const rt_float S = (Constants.k_B * Parameters.To )
                   /(mu * Constants.mass_p * pow(Data[j]->Density[i],Constants.gamma_ad-1.));
   Data[j]->Entropy[i] = S;
  }
 }

 return EXIT_SUCCESS;

}
