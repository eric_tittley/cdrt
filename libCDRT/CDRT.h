#ifndef _CDRT_H_
#define _CDRT_H_

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include "RT_Precision.h"
#include "RT_Data.h"
#include "RT_ConstantsT.h"

struct Parameters_struct {
 size_t NLOS;  /* Number of Lines of Sight (normally 1) */
 size_t NFLUX; /* Number of cells along the LoS */
 rt_float TimeUnit; /* s */
 rt_float TimeTotal;
 rt_float TimeDumpInterval;
 rt_float Length;
 rt_float DistanceFromSource;
 rt_float LengthUnit; /* m */
 rt_float Density; /* kg m^-3 */
 rt_float *Xo; /* Initial ionistation fractions. */
 size_t Xo_N; /* Number of array elements in Xo (should be 6) */
 rt_float To; /* Initial temperature [K] */
};
typedef struct Parameters_struct ParametersT;

enum ParameterEntries { iNLOS, iNFLUX, iTimeUnit, iTimeTotal, iTimeDumpInterval,
                        iLength, iDistanceFromSource, iLengthUnit, iDensity,
                        iXo, iTo, NParameterEntries };

int CDRT_ReadParameterFile(char const * const FileName,
                           ParametersT *Parameters);

int CDRT_SetR(rt_float const a_f,
              ParametersT const Parameters,
              RT_ConstantsT const Constants,
              RT_Data ** const Data);

int CDRT_SetRho(rt_float const z,
                rt_float const z_f,
                ParametersT const Parameters,
                RT_ConstantsT const Constants,
                RT_Data ** const Data);

int CDRT_SetT(ParametersT const Parameters,
              RT_ConstantsT const Constants,
              RT_Data ** const Data);

int CDRT_SetFractions(ParametersT const Parameters,
                      RT_Data ** const Data);

#endif
