#include <stdlib.h>

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include "RT_Precision.h"

#include "CDRT.h"
#include "RT_Data.h"

int CDRT_SetFractions(const ParametersT Parameters,
                      RT_Data **Data) {

 int i,j;

 for (j=0;j<Parameters.NLOS;j++) {
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->f_H1[i] =Parameters.Xo[0];
   Data[j]->f_H2[i] =Parameters.Xo[1];
   Data[j]->f_He1[i]=Parameters.Xo[2];
   Data[j]->f_He2[i]=Parameters.Xo[3];
   Data[j]->f_He3[i]=Parameters.Xo[4];
  }
 }

 return EXIT_SUCCESS;

}
