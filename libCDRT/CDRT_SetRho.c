#include <stdlib.h>
#include <math.h>

#include "CDRT.h"
#include "RT_Data.h"
#include "RT_ConstantsT.h"

/* Globals which should be passed in:
 * mass_p
 * mass_e
 */

int CDRT_SetRho(rt_float const z,
                rt_float const z_f,
                ParametersT const Parameters,
                RT_ConstantsT const Constants,
                RT_Data ** const Data) {


 /* Constants to try and cut down calculations in loop.
  * It would be nice if C permitted static definitions utilising other static
  * constants like C++ does. */
 const rt_float density_to_n_H = (1.0 - Constants.Y) / (Constants.mass_p + Constants.mass_e);
 const rt_float n_H_to_n_He = 0.25 * Constants.Y / (1.0 - Constants.Y); /* Nan if Y==1 */

 size_t i,j;
 /* The gas density */
 for (j=0;j<Parameters.NLOS;j++) {
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->Density[i]    = Parameters.Density;
  #ifdef CONSTANT_DENSITY_COMOVING
   Data[j]->Density[i]    *= pow(1+z_f,3);
  #endif
  }
 }

 /* The Hydrogen and Helium number densities */
 for (j=0;j<Parameters.NLOS;j++) {
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->n_H[i] = density_to_n_H * Data[j]->Density[i];
   if(Constants.Y > 0.) {
    if(Constants.Y < 1.) {
     Data[j]->n_He[i] = n_H_to_n_He * Data[j]->n_H[i];
    } else {
     /* Y = 1 */
     Data[j]->n_He[i] = Data[j]->Density[i] / ( 4.0 * (Constants.mass_p + Constants.mass_e) );
    }
   } else {
    Data[j]->n_He[i]=0.;
   }
  }
 }

 return EXIT_SUCCESS;

}
