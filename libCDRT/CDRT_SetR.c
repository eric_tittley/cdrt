#include <stdlib.h>

#include "CDRT.h"
#include "RT_Data.h"
#include "RT_ConstantsT.h"

int CDRT_SetR(rt_float const a_f,
              ParametersT const Parameters,
              RT_ConstantsT const Constants,
              RT_Data ** const Data) {

 /* Set the dimensions of a RT LOS.
 *  r0 is the distance from the source to the source-end of the LOS
 *  length is the length of the LOS. */
#ifdef CONSTANT_DENSITY_COMOVING
 /* aa is the expansion factor at the end of the iteration.
  * Strictly, R[*] should grow during a single integration. */
 const rt_float r0     = Parameters.DistanceFromSource  * a_f / Constants.h * Parameters.LengthUnit; /* m */
 const rt_float length = Parameters.Length * a_f / Constants.h * Parameters.LengthUnit; /* m */
#else
 const rt_float r0     = Parameters.DistanceFromSource * Parameters.LengthUnit; /* m */
 const rt_float length = Parameters.Length * Parameters.LengthUnit; /* m */
#endif

 /* Length of LOS segment (m) */
 const rt_float dR = length / Data[0]->NumCells;

 /* Find the proper distance of each cell from the source, using the
  * corrected length of a 1D slice given the redshift. (h^-1 comoving Mpc)
  * R[i] is the position of the lower edge of the cell. 
  * R[i]+dR is the position of the upper edge. */
 size_t i,j;
 for(j=0;j<Parameters.NLOS;j++) {
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->R[i] = r0 + ((rt_float)i)*dR;
  }
 }
 for(j=0;j<Parameters.NLOS;j++) {
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->dR[i] = dR;
  }
 }

 return EXIT_SUCCESS;

}
