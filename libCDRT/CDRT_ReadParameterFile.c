#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include "RT_Precision.h"
#include "CDRT.h"
#include "PF.h"

int CDRT_ReadParameterFile(char const * const FileName,
                           ParametersT * const Parameters) {

 /* Initialize Parameters with defaults */
 Parameters->NLOS               =1;
 Parameters->NFLUX              =256;
 Parameters->TimeTotal	        =0.;
 Parameters->TimeDumpInterval   =0.;
 Parameters->TimeUnit	        =0.;
 Parameters->Length	        =0.;
 Parameters->DistanceFromSource =0.;
 Parameters->LengthUnit	        =0.;
 Parameters->Density            =0.;
 Parameters->To                 =0.;

 /* Allocate memory */
 PF_ParameterEntry *ParameterEntries = (PF_ParameterEntry *)malloc(
                                         sizeof(PF_ParameterEntry)
                                        *(size_t)NParameterEntries );

/* Compile ParameterEntries structure */
 strncpy(ParameterEntries[iNLOS].Parameter, "NLOS", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iNLOS].Type      = UNSIGNED_LONG_INTEGER;
 ParameterEntries[iNLOS].Pointer   = &Parameters->NLOS;
 ParameterEntries[iNLOS].IsBoolean = 0;
 ParameterEntries[iNLOS].IsArray   = 0;

 strncpy(ParameterEntries[iNFLUX].Parameter, "NFLUX", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iNFLUX].Type      = UNSIGNED_LONG_INTEGER;
 ParameterEntries[iNFLUX].Pointer   = &Parameters->NFLUX;
 ParameterEntries[iNFLUX].IsBoolean = 0;
 ParameterEntries[iNFLUX].IsArray   = 0;

 strncpy(ParameterEntries[iTimeTotal].Parameter, "TimeTotal", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iTimeTotal].Type      = RT_FLOAT;
 ParameterEntries[iTimeTotal].Pointer   = &Parameters->TimeTotal;
 ParameterEntries[iTimeTotal].IsBoolean = 0;
 ParameterEntries[iTimeTotal].IsArray   = 0;

 strncpy(ParameterEntries[iTimeDumpInterval].Parameter, "TimeDumpInterval", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iTimeDumpInterval].Type      = RT_FLOAT;
 ParameterEntries[iTimeDumpInterval].Pointer   = &Parameters->TimeDumpInterval;
 ParameterEntries[iTimeDumpInterval].IsBoolean = 0;
 ParameterEntries[iTimeDumpInterval].IsArray   = 0;

 strncpy(ParameterEntries[iTimeUnit].Parameter, "TimeUnit", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iTimeUnit].Type      = RT_FLOAT;
 ParameterEntries[iTimeUnit].Pointer   = &Parameters->TimeUnit;
 ParameterEntries[iTimeUnit].IsBoolean = 0;
 ParameterEntries[iTimeUnit].IsArray   = 0;

 strncpy(ParameterEntries[iLength].Parameter, "Length", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iLength].Type      = RT_FLOAT;
 ParameterEntries[iLength].Pointer   = &Parameters->Length;
 ParameterEntries[iLength].IsBoolean = 0;
 ParameterEntries[iLength].IsArray   = 0;

 strncpy(ParameterEntries[iDistanceFromSource].Parameter, "DistanceFromSource", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iDistanceFromSource].Type      = RT_FLOAT;
 ParameterEntries[iDistanceFromSource].Pointer   = &Parameters->DistanceFromSource;
 ParameterEntries[iDistanceFromSource].IsBoolean = 0;
 ParameterEntries[iDistanceFromSource].IsArray   = 0;

 strncpy(ParameterEntries[iLengthUnit].Parameter, "LengthUnit", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iLengthUnit].Type      = RT_FLOAT;
 ParameterEntries[iLengthUnit].Pointer   = &Parameters->LengthUnit;
 ParameterEntries[iLengthUnit].IsBoolean = 0;
 ParameterEntries[iLengthUnit].IsArray   = 0;

 strncpy(ParameterEntries[iDensity].Parameter, "Density", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iDensity].Type      = RT_FLOAT;
 ParameterEntries[iDensity].Pointer   = &Parameters->Density;
 ParameterEntries[iDensity].IsBoolean = 0;
 ParameterEntries[iDensity].IsArray   = 0;

 strncpy(ParameterEntries[iXo].Parameter, "Xo", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iXo].Type      = RT_FLOAT;
 ParameterEntries[iXo].Pointer   = &Parameters->Xo;
 ParameterEntries[iXo].IsBoolean = 0;
 ParameterEntries[iXo].IsArray   = 1;
 ParameterEntries[iXo].NArrayElements = &Parameters->Xo_N;

 strncpy(ParameterEntries[iTo].Parameter, "To", MAX_PARAMETER_NAME_LENGTH);
 ParameterEntries[iTo].Type      = RT_FLOAT;
 ParameterEntries[iTo].Pointer   = &Parameters->To;
 ParameterEntries[iTo].IsBoolean = 0;
 ParameterEntries[iTo].IsArray   = 0;

/* Open parameter file for reading */
 FILE *File = fopen(FileName,"r");
 if(File == NULL) {
  printf("%s: %i: ERROR: failed to load file %s\n",__FILE__,__LINE__,FileName);
  return EXIT_FAILURE;
 }

 /* Read the parameters */
 int ierr = PF_ReadParameterFile(File, ParameterEntries, NParameterEntries);
 if(ierr!=EXIT_SUCCESS) {
  printf("%s: %i: ERROR: PF_ReadParameterFile failed\n",__FILE__,__LINE__);
  return EXIT_FAILURE;
 }

 /* Print the parameters using PF_WriteParameters */
 ierr = PF_WriteParameters(ParameterEntries, NParameterEntries);
 if(ierr!=EXIT_SUCCESS) {
  printf("%s: %i: ERROR: PF_WriteParameters failed\n",__FILE__,__LINE__);
  return EXIT_FAILURE;
 }

 /* Free the structure */
 free(ParameterEntries);

 /* Close the file */
 fclose(File);

 return EXIT_SUCCESS;
}
