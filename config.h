#ifndef _CONFIG_H_
#define _CONFIG_H_

/*** Code Control ***/

/* DEBUG traps and reports conditions that will lead to failure. Slows code
 * by about 1%, so safe to keep on. */
//#define DEBUG

/* VERBOSE outputs extra diagnostics to STDOUT.  ** Don't undefine ** Just set
 * to 0, 1, or 2. */
#define VERBOSE 0

/* DEBUG and VERBOSE together produce code with runtime about 30% longer */

/* RT_OPENMP turns on OpenMP parallelisations in the RT code. */
#define RT_OPENMP

/* RT_SERIAL compiles serial version of the RT code.  (dummy argument) */
//#define RT_SERIAL

/* CUDA turns on CUDA RT code.  Oddly, RT_SERIAL also needs to be set.
 * See also make.config */
//#define CUDA
/* Increase verbosity fot the CUDA code */
//#define CUDA_DEBUG

/* The Support/libSupport.a exists */
#define SUPPORT_EXISTS

/* Set the precision of the RT library.
 * If RT_DOUBLE not set, then all floats are single precision */
//#define RT_DOUBLE

/* #define HAVE_BOOL */

/* TIME_CODE_SEGMENTS provides coarse timing of the code sections. */
#define TIME_CODE_SEGMENTS

/* FORCE_REDSHIFT_STOP forces the simulation to stop at a given redshift,
 * independent of any other setting (it sets 'af'). This is not recommended.
 * Useful for running the PM simulation prior to any RT source turns on,
 * like when setting up initial conditions files (set BACKUPS if so). */
//#define FORCE_REDSHIFT_STOP 7.8

/* MAX_RT_ITERS limits the number of RT timesteps.  Useful only for testing.
 * DEFAULT should be commented out. */
//#define MAX_RT_ITERS 32768

/* NO_EXPANSION: by default, an expanding universe is assumed.
 * For testing purposes, the assumption of expansion can be turned off.
 * NOT FULLY IMPLEMENTED <- What the feck does that mean?
 * Currently, it turns off a correction in RT_LightFrontPosition */
#define NO_EXPANSION

/* Normalise the spectrum to produce a fixed number of ionising photons per
 * second (N_H_Ionising_photons_per_sec in constants.c) .  This overrules
 * whatever L_0 is set to in constants.c. Note, the spectrum is normalised
 * at z=100 so RT_Luminosity should give a result even if z_on < 100. */
/* #define SOURCE_NORMALISE_SPECTRUM */

/* ** Gas Density Field ** */
/* ** ONE OF READ_DENSITY_FILE, CONSTANT_DENSITY, or CALC_DENSITY must be set */
/* Define READ_DENSITY_FILE with the filename of a density file to be read
   on each pass.  Usefull for testing purposes, it advances the front through
   an unchanging field.
   Turns off CONSTANT_DENSITY and SPREAD_GAS_PARTICLE */
// #define READ_DENSITY_FILE "del_1_swap.dat"

/* CONSTANT_DENSITY sets the gas to a constant density kg/m^3 */
#define CONSTANT_DENSITY

/* CONSTANT_DENSITY_COMOVING unsets the fixed grid size set by
 * CONSTANT_DENSITY.  The density is then (aa^-3 * CONSTANT_DENSITY). */
 /* Not currently implemented properly */
//#define CONSTANT_DENSITY_COMOVING

/* Define REFINEMENTS if you wish to split a cell with a large optical depth
 * into many slices. See RT_constants for terms that control the conditions
 * of the refinement. */
#define REFINEMENTS

/* Define SEGMENTED_LOS if you intend to break lines of sight in parts, each
 * evolved independantly over the top-level timestep dt. */
//#define SEGMENTED_LOS

/* Define USE_MCTfR if you wish to limit the cells in which refinements are
 * done to cells to which there is a low (or limited) optical depth set by
 * Max_cumulative_tau_for_refinement in constants.c .
 * The solution is more "exact" if USE_MCTfR is *not* defined.  Defining
 * USE_MCTfR and setting Max_cumulative_tau_for_refinement to something large
 * like 2048 can halve the execution time.  Setting it even smaller makes the
 * code much faster, but tragically the result has a greater error because it
 * limits preheating of the gas in front of the I-front. Harder spectra suffer
 * more. */
#define USE_MCTfR

/* Thin medium approximation:  tau is assumed to be zero everywhere (the input
 * spectrum is not modified by absorption along the ray. You won't normally
 * want this. */
//#define THIN_APPROX

/* Define ADAPTIVE_TIMESCALE to allow the RT timescale to adaptively resolve
 * all important timescales.  If not defined, dt_RT = MIN_DT (set in
 * constants.c)  ADAPTIVE_TIMESCALE should ALWAYS be defined. Undefine for
 * testing purposes only (you can speed up the code dramatically by undefining
 * ADAPTIVE_TIMESCALE and setting MIN_DT to something large, like 1e12.) */
#define ADAPTIVE_TIMESCALE

/* Define USE_B_RECOMBINATION_CASE to use the alpha_B and beta_B
 * coefficients when the optical depth is large (or in a refinement),
 * otherwise alpha_a and beta_a are used. */
//#define USE_B_RECOMBINATION_CASE

/* USE_B_RECOMBINATION_CASE_ONLY is usually wrong. Here for testing purposes */
#define USE_B_RECOMBINATION_CASE_ONLY

/* If neither USE_B_RECOMBINATION_CASE nor USE_B_RECOMBINATION_CASE_ONLY
 * are defined, then the A coefficients are used exclusively */

/* Enforce a minimum temperature.  Useful if cooling excedes heating. */
//#define ENFORCE_MINIMUM_CMB_TEMPERATURE T_CMB

//#define CONSTANT_TEMPERATURE 1.0e4

/* Turn off cooling */
//#define NO_COOLING

/* For Cooling via Collisional Excitation of Neutral Hydrogen, use Spitzer's
 * approximation.
 * The default is Schulz & waters '91 */
//#define COOLING_COLLISIONAL_EXCITATION_OF_HYDROGEN__SPITZER

/*** End of Code Control ***/

/**** The ODE integration method ****/
/* One of the following must be defined. */

/* Use Euler's method: y => y + tau * dy/dt */
#define INTEGRATE_ODE_EULER

/* Use a fourth-order Runge-Kutta method */
//#define INTEGRATE_ODE_RUNGE_KUTTA

/* Uncomment to store the Ionizaitiona and Recombination rates in the output
 * data file. */
#define RT_DATA_RATES

/* Print the redshift/time unit to 6 digits, not 2 */
//#define LONG_FILE_LABEL

/* Check whether we are doing any MPI at all */
#if (!defined RT_MPI) && (defined CONSTANT_DENSITY)
# define NO_MPI
#endif

#define MASTER 0

#endif /* _CONFIG_H_ */
